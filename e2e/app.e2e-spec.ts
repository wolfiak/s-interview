import { SamsungPage } from './app.po';

describe('samsung App', () => {
  let page: SamsungPage;

  beforeEach(() => {
    page = new SamsungPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
