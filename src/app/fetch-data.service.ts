import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class FetchDataService {
  constructor(private http: Http) {}

  getData() {
    return this.http
      .get('https://jsonplaceholder.typicode.com/posts')
      .map(res => {
        console.log(res);
        return res.json();
      });
  }
  postData(obekit) {
    return this.http.post('https://jsonplaceholder.typicode.com/posts', obekit).map(res => {
      console.log('Post data ', res);
      return true;
    });
  }
}
