import { Component, OnInit } from '@angular/core';
import { FetchDataService } from '../fetch-data.service';

@Component({
  selector: 'app-dodaj',
  templateUrl: './dodaj.component.html',
  styleUrls: ['./dodaj.component.scss']
})
export class DodajComponent implements OnInit {
  title: string;
  body: string;
  constructor(private f: FetchDataService) {}

  ngOnInit() {}

  walidacja(event) {
    console.log(event);
  }
  kliko(event) {
    console.log('Klinknieto');
    console.log(event);
    const obiket = {
      userId: 1,
      id: new Date(),
      title: this.title,
      body: this.body
    };
    this.f.postData(obiket).subscribe(res => {
      console.log(res);
    });
  }
}
