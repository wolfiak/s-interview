import { Component, OnInit } from '@angular/core';
import { FetchDataService } from '../fetch-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  objecto: any[];
  constructor(private fetchS: FetchDataService) {}

  ngOnInit() {
    this.fetchS.getData().subscribe(res => {
      console.log(res);
      this.objecto = res;
    });
  }
}
