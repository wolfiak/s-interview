import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FetchDataService } from './fetch-data.service';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { SharedModule } from './shared/shared.module';
import { HttpModule } from '@angular/http';
import { DodajComponent } from './dodaj/dodaj.component';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [AppComponent, HomeComponent, ToolbarComponent, DodajComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpModule,
    FormsModule
  ],
  providers: [FetchDataService],
  bootstrap: [AppComponent]
})
export class AppModule {}
